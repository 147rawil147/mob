var syntax = "scss", // Syntax: sass or scss;
  gulpVersion = "4"; // Gulp version: 3 or 4
gmWatch = false; // ON/OFF GraphicsMagick watching "img/_src" folder (true/false). Linux install gm: sudo apt update; sudo apt install graphicsmagick

var gulp = require("gulp"),
  gutil = require("gulp-util"),
  sass = require("gulp-sass"),
  browserSync = require("browser-sync"),
  concat = require("gulp-concat"),
  uglify = require("gulp-uglify"),
  cleancss = require("gulp-clean-css"),
  rename = require("gulp-rename"),
  autoprefixer = require("gulp-autoprefixer"),
  notify = require("gulp-notify"),
  rsync = require("gulp-rsync"),
  imageResize = require("gulp-image-resize"),
  imagemin = require("gulp-imagemin"),
  del = require("del");

// Local Server
gulp.task("browser-sync", function () {
  browserSync({
    server: {
			baseDir: 'assets'
		},
    notify: false,
    // open: false,
    // online: false, // Work Offline Without Internet Connection
    // tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
  });
});

// Sass|Scss Styles
gulp.task("styles", function () {
  return (
    gulp
      .src("assets/" + syntax + "/**/*." + syntax + "")
      .pipe(sass({ outputStyle: "expanded" }).on("error", notify.onError()))
      //  .pipe(rename({ suffix: ".min", prefix: "" }))
      .pipe(autoprefixer(["last 15 versions"]))
      //  .pipe(cleancss({ level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
      .pipe(gulp.dest("assets/css"))
  );
});

// If Gulp Version 4
if (gulpVersion == 4) {
  gulp.task("watch", function () {
    gulp
      .watch(
        "assets/" + syntax + "/**/*." + syntax + "",
        gulp.parallel("styles")
      )
      .on("change", browserSync.reload);
    gulp.watch("../**/*.php").on("change", browserSync.reload);
  });
  gmWatch
    ? gulp.task("default", gulp.parallel("styles", "watch", "browser-sync"))
    : gulp.task("default", gulp.parallel("styles", "watch", "browser-sync"));
}
