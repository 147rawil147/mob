$(function () {

	//================ header
	$(document).scroll(function () {
		500 < $(document).scrollTop() ? $(".header-mob").addClass("h-fixed") : $(".header-mob").removeClass("h-fixed");
	})

	//================ menu-burger
	$("#burger").on("click", function (e) {
		$(this).toggleClass("active");
		$("#headerMobContent").toggleClass("active");
		$("body").toggleClass("overflow-h");
	});

	//=============== accordeon
	// $('.head-wrap').on('click', function () {
	// 	$('.head').removeClass('head-active');
	// 	$(this).parent().addClass('head-active');
	// });


	$('.head-wrap').on('click', function () {

		if ($(this).parent().hasClass('head-active')) {
			$('.head').removeClass('head-active');
		} else {
			$('.head').removeClass('head-active');
			$(this).parent().addClass('head-active');
			let prices = $(this).data('price');
			if (prices) {
				var priceCont = prices.split(' ');
				priceCont.forEach(function (item, i, arr) {
					$('#price-accordeon').find('.head[data-price="' + item + '"]').addClass('head-active');
				});
			}
		}
	});


	//================ sliders
	$('.baseSlider').slick({
		slidesToShow: 1,
		dots: true,
		arrows: false,
	});


	$('#teamSlider').slick({
		slidesToShow: 1,
		dots: true,
		arrows: false,
		infinite: true,
		speed: 400,
		fade: true,
		cssEase: 'linear',
		adaptiveHeight: true
	});

	$('#tour-slider').slick({
		slidesToShow: 1,
		dots: true,
		arrows: false,
	});

	$('.advantages-slider').slick({
		prevArrow: '<button class="advantages-slider__errow advantages-slider__errow-left"> <img src="./img/gold-errow-left.svg" alt=""></button>',
		nextArrow: '<button class="advantages-slider__errow advantages-slider__errow-right"> <img src="./img/gold-errow-right.svg" alt=""></button>',
		infinite: false,
	});

	//================ FORM-GROUP-SELECT
	$('.select').each(function () {
		const _this = $(this),
			selectOption = _this.find('option'),
			selectOptionLength = selectOption.length,
			selectedOption = selectOption.filter(':selected'),
			duration = 450; // длительность анимации 

		_this.hide();
		_this.wrap('<div class="select"></div>');
		$('<div>', {
			class: 'new-select',
			text: _this.children('option:disabled').text()
		}).insertAfter(_this);

		const selectHead = _this.next('.new-select');
		$('<div>', {
			class: 'new-select__list'
		}).insertAfter(selectHead);

		const selectList = selectHead.next('.new-select__list');
		for (let i = 1; i < selectOptionLength; i++) {
			$('<div>', {
				class: 'new-select__item',
				html: $('<span>', {
					text: selectOption.eq(i).text()
				})
			})
				.attr('data-value', selectOption.eq(i).val())
				.appendTo(selectList);
		}

		const selectItem = selectList.find('.new-select__item');
		selectList.slideUp(0);
		selectHead.on('click', function () {
			if (!$(this).hasClass('on')) {
				$(this).addClass('on');
				selectList.slideDown(duration);

				selectItem.on('click', function () {
					let chooseItem = $(this).data('value');

					$('select').val(chooseItem).attr('selected', 'selected');
					selectHead.text($(this).find('span').text());

					selectList.slideUp(duration);
					selectHead.removeClass('on');
				});

			} else {
				$(this).removeClass('on');
				selectList.slideUp(duration);
			}
		});
	});

	$('#school-select').click(function () {
		$('#tour-slider').slick('slickGoTo', $(this).find('select').val());
	});

	//===================== popup
	$('.popup-btn').on('click', function (event) {
		event.preventDefault();
		showPopup();
	})
	$('.popup-btn__closed').on('click', function (event) {
		event.preventDefault();
		hidePopup();
	})

	$(document).on("click", function(e) {
		if (($(e.target).parent(".popup__inner")
			|| $(e.target).hasClass(".popup__inner"))
			&& $(e.target).parent(".popup").css("display") == "block"
		) {
			hidePopup();
		}
	})

	function hidePopup() {
		$('.popup').fadeOut();
		$('.popup-btn').removeClass('hidden');
	}
	function showPopup() {
		$('.popup').fadeIn();
		$('.popup-btn').addClass('hidden');
	}

	//==================== play-video
	$('.video-wrap').on('click', '.play-js', function () {
		let video = $(this).closest(".video-wrap").find('.video');
		if ($(this).hasClass('play')) {
			video.trigger('play');
			$(this).removeClass("play").addClass("pause");
		} else {
			video.trigger('pause');
			$(this).removeClass("pause").addClass("play");
		}
	})


	//===================== MAP
	var obj = [
		{ "address": "Балканская площадь 5И (Балканский-1), 7 этаж", "lat": "59.828849", "lng": "30.379747" },
		{ "address": "Ленинский проспект 119, к. 5, 3 этаж", "lat": "59.851459", "lng": "30.257483" },
		{ "address": "Проспект Науки 10, к. 1", "lat": "60.015144", "lng": "30.390585" },
		{ "address": "Ул. Звёздная, дом 1 Бизнес-центр, 2 этаж, офис 201", "lat": "59.832618", "lng": "30.347585" },
		{ "address": "Бизнес-центр, ул. Осипенко, дом 2 (пересечение Индустриального и Косыгина)", "lat": "59.945496", "lng": "30.483460" },
		{ "address": "ТРК Вояж, проспект Энгельса, дом 124, к 1, 3 этаж", "lat": "60.039243", "lng": "30.323494" },
	];
	var locations = [];
	obj.map((el, index) => {
		locations.push([el.address, Number(el.lat), Number(el.lng), index]);
	});
	var lat = locations[0][1]
	var lng = locations[0][2]
	mapInit(lat, lng);
	function mapInit(lat, lng, listNumber) {

		var infowindow = new google.maps.InfoWindow();
		var marker, i;


		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 10,
			mapTypeControlOptions: {
				mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
					'styled_map']
			},

			styles: [
				{
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#f5f5f5"
						}
					]
				},
				{
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#616161"
						}
					]
				},
				{
					"elementType": "labels.text.stroke",
					"stylers": [
						{
							"color": "#f5f5f5"
						}
					]
				},
				{
					"featureType": "administrative.land_parcel",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#bdbdbd"
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#eeeeee"
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#757575"
						}
					]
				},
				{
					"featureType": "poi.park",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#e5e5e5"
						}
					]
				},
				{
					"featureType": "poi.park",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#9e9e9e"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#ffffff"
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#757575"
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#dadada"
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#616161"
						}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#9e9e9e"
						}
					]
				},
				{
					"featureType": "transit.line",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#e5e5e5"
						}
					]
				},
				{
					"featureType": "transit.station",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#eeeeee"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#c9c9c9"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels.text.fill",
					"stylers": [
						{
							"color": "#9e9e9e"
						}
					]
				}
			],

			center: new google.maps.LatLng(lat, lng),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		if (listNumber != undefined) {
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 15,
				center: new google.maps.LatLng(lat, lng),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				styles: [
					{
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#f5f5f5"
							}
						]
					},
					{
						"elementType": "labels.icon",
						"stylers": [
							{
								"visibility": "off"
							}
						]
					},
					{
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#616161"
							}
						]
					},
					{
						"elementType": "labels.text.stroke",
						"stylers": [
							{
								"color": "#f5f5f5"
							}
						]
					},
					{
						"featureType": "administrative.land_parcel",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#bdbdbd"
							}
						]
					},
					{
						"featureType": "poi",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#eeeeee"
							}
						]
					},
					{
						"featureType": "poi",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#757575"
							}
						]
					},
					{
						"featureType": "poi.park",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#e5e5e5"
							}
						]
					},
					{
						"featureType": "poi.park",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#9e9e9e"
							}
						]
					},
					{
						"featureType": "road",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#ffffff"
							}
						]
					},
					{
						"featureType": "road.arterial",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#757575"
							}
						]
					},
					{
						"featureType": "road.highway",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#dadada"
							}
						]
					},
					{
						"featureType": "road.highway",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#616161"
							}
						]
					},
					{
						"featureType": "road.local",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#9e9e9e"
							}
						]
					},
					{
						"featureType": "transit.line",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#e5e5e5"
							}
						]
					},
					{
						"featureType": "transit.station",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#eeeeee"
							}
						]
					},
					{
						"featureType": "water",
						"elementType": "geometry",
						"stylers": [
							{
								"color": "#c9c9c9"
							}
						]
					},
					{
						"featureType": "water",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#9e9e9e"
							}
						]
					}
				],
			});
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[listNumber][1], locations[listNumber][2]),
				map: map,
				flat: true,
				icon: './img/map-marker.svg',
				animation: google.maps.Animation.DROP,

			});

			infowindow.setContent(locations[listNumber][0]);
			infowindow.open(map, marker);
		}
		for (i = 0; i < locations.length; i++) {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				map: map,
				flat: true,
				icon: './img/map-marker.svg',
				animation: google.maps.Animation.DROP,
			});

		}
		$('#map-select').click(function () {
			var listNumber = $(this).find('select').val();
			var lat = locations[listNumber][1];
			var lng = locations[listNumber][2];
			mapInit(lat, lng, listNumber);
			$('.addresses-contacts_tab').hide();
			$('.addresses-contacts_tab[data-tab="' + listNumber + '"]').show();
			$('.addresses__slider').slick('slickGoTo', $(this).find('select').val());
		});
	};
});